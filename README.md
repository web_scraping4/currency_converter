# Currency Converter
Script that converts a given amount of money from one currency to another.
# Setup (linux)
- Inside the root directory (`currency_converter`), install `requirements.txt`:
    ```shell
    python -m venv venv && \
    source venv/bin/activate && \
    pip install -r requirements.txt
    ```
- Run the program
    ```shell
    python currency_converter.py
    ```
