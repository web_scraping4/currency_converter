import requests

msg_currency_from = "from which currency do you want to convert (usd, eur, gbp, etc.): "
msg_currency_to = "to which currency do you want to convert (usd, eur, gbp, etc.): "
msg_amount = "Enter the amount of money "
msg_checking = "Checking the cache..."
msg_in_cache = "Oh! It is in the cache!"
msg_not_in_cache = "Sorry, but it is not in the cache!"


def request_json_exchange_rates(currency) -> dict:
    """
    returns a dictionary with the exchange rates of the specified currency
    :param currency: (string) the currency code in lowercase
    :returns dictionary with the exchange rates of the currency
    :raises ValueError
    """
    currency = currency.lower()
    url = f"https://www.floatrates.com/daily/{currency}.json"
    response = requests.get(url)
    if response.status_code != 200:
        raise ValueError(f"bad currency code, response status code: {response.status_code}")
    return response.json()


def fill_cache(cache, currency) -> None:
    """fills the cache with the most used currencies"""
    currency = currency.lower()
    currency_object = request_json_exchange_rates(currency)
    if currency != "usd":
        cache["usd"] = currency_object["usd"]["rate"]
    if currency != "eur":
        cache["eur"] = currency_object["eur"]["rate"]


def update_cache(cache, currency_from, currency_to) -> None:
    """
    updates the cache
    :param currency_from
    :param currency_to
    :raises ValueError
    """
    currency_object = request_json_exchange_rates(currency_from)
    if currency_to.lower() not in currency_object:
        raise ValueError("Bad currency code")
    cache[currency_to.lower()] = currency_object[currency_to.lower()]["rate"]


if __name__ == "__main__":
    currency_cache = {}
    try:
        currency_from = input(msg_currency_from)
        fill_cache(currency_cache, currency_from)
        while currency_to := input(msg_currency_to).lower():
            amount = int(input(msg_amount))
            exchange = 0
            result = 0
            print(msg_checking)
            if currency_to in currency_cache:
                print(msg_in_cache)
                exchange = currency_cache[currency_to]
            else:
                print(msg_not_in_cache)
                update_cache(currency_cache, currency_from, currency_to)
                exchange = request_json_exchange_rates(currency_from)[currency_to]["rate"]
            result = round(amount * exchange, 2)
            print(f"You received {result} {currency_to.upper()}.")
    except ValueError as e:
        print(e)
